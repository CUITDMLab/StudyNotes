# 主页
>这个库是汇总本实验室学生所有的学习经验，希望可以避免未来的师弟们不用再踩重复的坑，还有是希望为大家都可以养成做笔记的习惯。
>现在的链接都是跳转到别的网站，希望到后来逐渐的会把所有链接的跳转，都指向这个库内的文件。

## 数据挖掘

![选择正确的评估器.jpg](/images/README/选择正确的评估器.jpg)

### 1.1常用算法

[机器学习算法python实现](https://github.com/lawlite19/MachineLearning_Python)

[数据方向学习笔记](http://nbviewer.jupyter.org/github/lijin-THU/notes-python/blob/master/index.ipynb)

[数据类别不平衡](https://github.com/scikit-learn-contrib/imbalanced-learn)

### 1.2深度学习

### 1.3基因表达式

## 强化学习

[David Silver强化学习公开课](https://chenrudan.github.io/categories/project-experience/)

[强化学习相关算法的代码实现](https://github.com/dennybritz/reinforcement-learning)

## 分布式

Hadoop的搭建

## 英语

## 面试

[用python实现数据结构，等面试题](https://github.com/Jack-Lee-Hiter/AlgorithmsByPython)

## 最近读的论文